"""

Joseph Pasquantonio
Shelley Zhang
CIS 522 - Algorithms and Complexity
Homework 3
Question 2
Date: 2/23/16

"""

# class for creating contestant objects
class Contestant:

    def __init__(self, n, st, bt, rt):
        self._name = n
        self._swim_time = st
        self._bike_time = bt
        self._run_time = rt
        self._bikerun = bt + rt
        self._total_time = st + bt + rt

    @property
    def name(self):
        return self._name
    
    @property
    def swim_time(self):
        return self._swim_time

    @swim_time.setter
    def swim_time(self, s):
        self._swim_time = s
        
    @property
    def bike_time(self):
        return self._bike_time
    
    @property
    def run_time(self):
        return self._run_time

    @property
    def bikerun(self):
        return self._bikerun

    @bikerun.setter
    def bikerun(self, t):
        self._bikerun = t
        
    @property
    def total_time(self):
        return self._total_time

    def __str__(self):
        print("-------------------------")
        print("Name: {n}".format(n = self.name))
        #print("Swim time: {s}".format(s = self.swim_time))
        #print("Bike time: {b}".format(b = self.bike_time))
        #print("Run  time: {r}".format(r = self.run_time))
        print("Bike + Run: {br}".format(br = self.bikerun))
        #print("Total time: {t}".format(t = self.total_time))
        print("-------------------------")
        
# create schedule using merge sort implementation.. sort by bike time + run time
def merge_sort(people):
    result = []
    if len(people) < 2:
        return people
    mid = int(len(people)/2)
    left = merge_sort(people[:mid])
    right = merge_sort(people[mid:])
    i = 0
    j = 0
    while i < len(left) and j < len(right):
            # reverse sort by swapping the < with > and vice versa
            if left[i].bikerun < right[j].bikerun:
                result.append(right[j])
                j += 1
            else:
                result.append(left[i])
                i += 1
    result += left[i:]
    result += right[j:]
    return result

# print info about contestants
def __info__(people):
    for p in people:
        p.__str__()

def e_c_t(p):
    st = 0
    br = 0
    for a in p:
        st += a.swim_time
        br = a.bikerun
    total = st + br
    return total
        
# main method 
if __name__ == "__main__":

    # create as many contestants as you want
    # template = Contestant(name, swim time, bike time, run time)
    a = Contestant('a', 5, 5, 3)
    b = Contestant('b', 4, 1, 1)
    c = Contestant('c', 3, 3, 1)
    d = Contestant('d', 2, 3, 2)
    e = Contestant('e', 1, 1, 2)

    # add contestants to a list
    contestants = [a,b,c,d,e]

    # print information about each contestant
    #__info__(contestants)

    # create schedule for contestants
    schedule = merge_sort(contestants)

    # print the schedule (order) in which contestants will start race
    print("********************************")
    print("Schedule: ", end=" ")
    for p in schedule: print(p.name, end=" ")
    print()
    print("********************************")
    print("-------------------------")
    print("-------------------------")
    print("Contestant Info Below: ")
    print("-------------------------")
    for p in schedule:
        p.__str__()
    print("-------------------------")
    print("Earliest Completion Time: " + str(e_c_t(schedule)))
    
