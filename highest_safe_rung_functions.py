"""
Joseph Pasquantonio

Homework 2
Question 1 part b

"""
import math
from random import randint

""" Non Recursive function with the assumption that k = 2"""
def find_highest(breakpoint, n):
    step_increment = int(round(math.sqrt(n),0))
    current_step = step_increment
    previous_step = 0

    broken1 = False
    broken2 = False
    while not broken1:
        if current_step < breakpoint:
            previous_step = current_step
            current_step = current_step + step_increment
        else:
            broken1 = True
            current_step = previous_step + 1
            while not broken2:
                if current_step < breakpoint:
                    current_step += 1
                else:
                    broken2 = True
                    highest_safe_rung = current_step - 1
                    return highest_safe_rung

""" Recursive function with the assumption that k > 2 """
def find_rung(start, breakpoint, n, k):
    #breakpoint = 1072
    step_increment = int(round((math.pow(n, (k - 1)/k)), 0))
    print('STEP SIZE: ' + str(step_increment))
    current_step = start + step_increment
    previous_step = start
    broken = False
    if k == 1:
        print('k = 1')
        while not broken:
            print(start)
            if start == breakpoint:
                print('return start - 1')
                return start - 1
            else:
                start += 1
    else:
        while True:
            if current_step < breakpoint:
                previous_step = current_step
                current_step = current_step + step_increment
            else:
                return find_rung(previous_step, breakpoint,(current_step - previous_step), (k - 1))


breakpoint = randint(1,1000)
print(find_rung(0,breakpoint,1000,3))
print(find_highest(breakpoint, 1000))
print(breakpoint)
