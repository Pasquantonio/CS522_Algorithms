"""
Joseph Pasquantonio
CIS 522: Advanced Algorithm Design and Complexity
Shelley Zhang
January 30, 2016

Homework #1, Question #2
Implementation of Propose and Reject Algorithms
Reference: Algorithm Design by Jon Kleinbeg and Eva Tardos
           GS 'Propose and Reject' algorithm page: 6
"""
class Person:
    
    def __init__(self, r, n, m, f, l, nl):
        self._number = r
        self._name = n
        self._match = m
        self._free = f
        self._list = l
        self._numerical_list = nl
        self._count = []
        self._list_of_men = [1,2,3,4,5] 
        #self._list_of_women = [1,2,3,4,5] # only use if making women the proposers

    @property
    def man_ranks(self):
        return self._man_ranks
    
    @property
    def number(self):
        return self._number
    
    @number.setter
    def number(self, n):
        self._number = n

    @number.deleter
    def number(self):
        del self._number
        
    @property
    def name(self):
        return self._name
    
    @name.setter
    def name(self, n):
        self._name = n

    @name.deleter
    def name(self):
        del self._name

    @property
    def match(self):
        return self._match

    @match.setter
    def match(self, m):
        self._match = m

    @match.deleter
    def match(self):
        del self._match

    @property
    def free(self):
        return self._free

    @free.setter
    def free(self, f):
        self._free = f

    @free.deleter
    def free(self):
        del self._free

    @property
    def list(self):
        return self._list

    @list.setter
    def list(self, l):
        self._list = l

    @list.deleter
    def list(self):
        del self._list

    @property
    def numerical_list(self):
        return self._numerical_list

    @numerical_list.setter
    def numerical_list(self, nl):
        self._numerical_list = nl

    @numerical_list.deleter
    def numerical_list(self):
        del self._numerical_list
        
    @property
    def count(self):
        return self._count

    @count.setter
    def count(self, a):
        self._count = a

    @count.deleter
    def count(self):
        del self._count

    def match_print(self):
        print(self.name, self.match)
    
    def add_count(self, name):
        self.count.append(name)
        print('{this} Proposal Attempts: {attempts}'.format(this = self.name, w = name, attempts = self.count))

    def choose_highest(self):
        if self.list:
            return self.list[0]
        else:
            print('{person} has proposed to every woman'.format(person = self.name))
            return None
        
    def print_list(self):
        for name in self.list:
            print(name, end=' ')
        print()

    def sort_numerical(self):
        sorted(self.numerical_list, key=lambda x: x[1])
        for i in range(0, len(self.numerical_list)):
            self.numerical_list[i] = self.numerical_list[i][1]

# global functions
def free_exists(men, women):
    if men:
        if more_possible_proposals(m, women):
            return (True, men[0])
    else:
        print('No Free men')
        return (False, None)

def more_possible_proposals(m, women):
    if m.list:
        return True
    else:
        return False

def engagement(m, w, men, taken, wife, husband):
    w.free = False
    m.free = False
    m.add_count(m.list.pop(m.list.index(w.name)))
    #print('Women left ' + str(m.list))
    w.add_count(m.name)
    m.match = w.name
    w.match = m.name
    wife[m.number] = w.number
    husband[w.number] = m.number
    engaged.append(m.name)
    engaged.append(w.name)
    print('Matched: ({m},{w})'.format(m = m.name, w = w.name))
    print(m.name + ' added to taken list')
    taken.append(m)
    men.pop(men.index(m))
    """print('Men in men list')
    for m in men:
        print(m.name)
    print('-------------')
    print('Men in taken list')
    for m in taken:
        print(m.name)
    print('-------------')"""
    
def rekt(m, w):
    print('{name} remains free'.format(name = m.name))
    m.add_count(m.list.pop(m.list.index(w.name)))
    w.add_count(m.name)

def dumped(m, w, engaged, men, taken):
    print('{m} is now free'.format(m = m.name))
    engaged.remove(m.name)
    engaged.remove(w.name)
    m.free = True
    w.free = True
    men.append(m)
    # This sort is not necesarry but it seems like the algorithm did this
    men.sort(key=lambda n: n.name, reverse=False)
    taken.pop(taken.index(m))
    """print('Men in men list')
    for m in men:
        print(m.name)
    print('-------------')
    print('Men in taken list')
    for m in taken:
        print(m.name)
    print('-------------')"""
    
def find_man(w, taken):
    for m in taken:
        if m.name == w.match:
            return m
def choose_woman(name, women):
    for woman in women:
            if woman.name == name:
                return woman
            
# main 
if __name__ == "__main__":    
    v = Person(1,'V', 0, True, ['B','A','D','E','C'], [(1,2),(2,1),(3,4),(4,5),(5,3)])
    w = Person(2,'W', 0, True, ['D','B','A','C','E'], [(1,4),(2,2),(3,1),(4,3),(5,5)])
    x = Person(3,'X', 0, True, ['B','E','C','D','A'], [(1,2),(2,5),(3,3),(4,4),(5,1)])
    y = Person(4,'Y', 0, True, ['A','D','C','B','E'], [(1,1),(2,4),(3,3),(4,2),(5,5)])
    z = Person(5,'Z', 0, True, ['B','D','A','E','C'], [(1,2),(2,4),(3,1),(4,5),(5,3)])

    a = Person(1,'A', 0, True, ['Z','V','W','Y','X'], [(1,5),(2,1),(3,2),(4,4),(5,3)])
    b = Person(2,'B', 0, True, ['X','W','Y','V','Z'], [(1,3),(2,2),(3,4),(4,1),(5,5)])
    c = Person(3,'C', 0, True, ['W','X','Y','Z','V'], [(1,2),(2,3),(3,4),(4,5),(5,1)])
    d = Person(4,'D', 0, True, ['V','Z','Y','X','W'], [(1,1),(2,5),(3,4),(4,3),(5,2)])
    e = Person(5,'E', 0, True, ['Y','W','Z','X','V'], [(1,4),(2,2),(3,5),(4,3),(5,1)])

    # to have woman be the ones proposing just put them in the men array & men in women array
    #women = [v,w,x,y,z]
    #men = [z,y,x,w,v]
    men = [v,w,x,y,z]
    taken = []
    #men = [a,b,c,d,e]
    women = [a,b,c,d,e]
    
    for m in men:
        print(m.name, end=': ')
        m.print_list()
    print()

    for w in women:
        print(w.name, end = ': ')
        w.sort_numerical()
        w.print_list()
    print()
    
    engaged = []
    wife = [0,0,0,0,0,0]
    husband = [0,0,0,0,0,0]
    
    # While there is still a free man who hasnt proposed to every girl
    man_free = True
    while man_free:
        # choose such a man m
        (man_free, m) = free_exists(men, women)
        if man_free == False:
            break
        # Let w be the highest-ranked woman in m's preference list to whom m has not yet proposed
        w = choose_woman(m.choose_highest(), women)
        # use w (name) to get the correct woman object
        print('{m} proposes to {w}'.format(m = m.name, w = w.name))
        # If w is free then (m,w) become engaged
        if w.free == True:
            engagement(m, w, men, taken, wife, husband)
        # Else w is currently engaged to m_prime
        else:
            # assign m_prime as w's current match
            m_prime = find_man(w, taken)
            # if w prefers m_prime to m then m remains free
            if w.list.index(m_prime.name) < w.list.index(m.name):
                # m remains free
                rekt(m, w)
            # else w prefers m to m_prime
            else:
                dumped(m_prime, w, engaged, men, taken)
                engagement(m, w, men, taken, wife, husband)
            
    for man in taken:
        man.match_print()

    print('Wife: '+ str(wife[1:]))
    print('Husband: ' + str(husband[1:]))
