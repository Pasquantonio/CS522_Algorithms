"""
Joseph Pasquantonio
Shelley Zhang
Algorithms and Complexity CIS 522
Homework 4

Chapter 5 Exercise 2
"""
import math
import sys
import random

# very similar to last weeks homework but added counting functionality
def merge_count(l, r):
    arr = []
    counter = 0
    i = 0
    j = 0
    n_l = len(l)
    n_r = len(r)
    
    while i < n_l and j < n_r:
        # check inversion based on significant inversion definition
        if (r[j]*2) < l[i]:
            #print(l[i], r[j]*2)
            #print("inversion")
            # increase count
            counter = counter + (n_l - i)
            #increment j
            j = j + 1
        else:
            #increment i
            i = i + 1
        #endif
            
    # reset i and j
    i = 0
    j = 0

    while i < n_l and j < n_r:
        if r[j] > l[i]:
            tmp = l[i]
            # append to new array
            arr.append(tmp)
            i = i + 1
        else:
            tmp = r[j]
            # append to new array
            arr.append(tmp)
            j = j + 1

    while l[i:] or r[j:]:
        if r[j:]:
            tmp = r[j]
            # append to new array
            arr.append(tmp)
            j = j + 1
        if l[i:]:
            tmp = l[i]
            # append to new array
            arr.append(tmp)
            i = i + 1
    return arr, counter

# Main inversion method
# inversion is recursive but also makes a call to merge and count method
def inversions(sequence):
    # set n equal to the length of the input sequence
    n = len(sequence)
    # if the sequence is less than 2 return it and zero inversions because only 1 or 0 elements
    if n < 2:
        return sequence, 0
    # if n is > than 2
    else:
        # grab middle of the sequence
        mid = math.floor(int(n/2))
        # recursive right side call
        right, y = inversions(sequence[mid:])
        #print('Right = ' + str(right))
        #print('Y = ' + str(y))
        # recursive left size call
        left, x = inversions(sequence[:mid])
        #print('Left = ' + str(left))
        #print('X = ' + str(x))
        # merge and count inversions
        end, z = merge_count(left, right)
        #print('Z = ' + str(z))
        # add up total inversions
        x = x + y
        z = z + x
        return end, z

# creating test cases 3 static, 2 randomly generated using import random
t1 = [10, 9, 8, 7, 6, 5, 4, 3, 2, 1]
t2 = [9, 5, 6, 2, 1]
t3 = [5, 6, 2, 1, 4, 10, 3]
t4 = random.sample(range(11), 10)
t5 = random.sample(range(30), 20)
# add test cases to list for easy execution
sequences = [t1, t2, t3, t4, t5]
print("Test Results for 5 Tests (test4 and test5 are randomly generated)")
print("-----------------------------------------------------------------")
for i in range(0, len(sequences)):
    print("Sequence {t} = {s}".format(t = "test"+ str(i), s = str(sequences[i]))) 
    e, t = inversions(sequences[i])
    print("Sorted: {e}".format(e = e))
    print("Inversions: {i}".format(i = t))
    print("-------------------------------------------")
